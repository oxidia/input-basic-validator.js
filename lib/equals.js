const stringTypeError = require('./tools/stringTypeError');

function equals(a, b, options = null) {
	stringTypeError(a);
	stringTypeError(b);

	options = options || {};
	let case_sensitive = options.case_sensitive === false ? false : true;

	if (case_sensitive === false) {
		a = a.toLowerCase();
		b = b.toLowerCase();
	}
	return (a.toString() === b.toString());
}

module.exports = equals;
