const typeError = require('./typeError');

function stringTypeError(received) {
	if (typeof(received) !== 'string' && !(received instanceof String))
		typeError('string', received);
}

module.exports = stringTypeError;
