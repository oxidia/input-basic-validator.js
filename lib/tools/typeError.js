function typeError(expected, received) {
	if (received === null)
		received = 'null';
	else
		received = typeof(received);
	return ('Expected ' + expected + ', received ' + received + '.');
}

module.exports = typeError;
