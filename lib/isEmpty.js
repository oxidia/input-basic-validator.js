const stringTypeError = require('./tools/stringTypeError');

function isEmpty(str, options = null) {
	stringTypeError(str);

	options = options || {};
	let	ignore_space = options.ignore_space || false;

	if (ignore_space === true)
		str = str.trim();
	return (str.length === 0);
}

module.exports = isEmpty;
