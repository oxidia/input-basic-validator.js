const stringTypeError = require('./tools/stringTypeError');

function isNumeric(str, options = null) {
	stringTypeError(str);

	let	regex;
	options = options || {};
	let	no_symbols = options.no_symbols || false;

	regex = (no_symbols === true) ? /^[0-9]+$/ : /^[+-]?(?:[0-9]*\.[0-9]+|[0-9]+(?:\.[0-9]+)?)$/;
	return (regex.test(str));
}

module.exports = isNumeric;
