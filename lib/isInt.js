const stringTypeError = require('./tools/stringTypeError');
const regex = /^[+-]?[0-9]+$/;

function isInt(str, options = null) {
	stringTypeError(str);

	options = options || {};
	let	checkLTE = !options.hasOwnProperty('lte') || str <= options.lte;
	let	checkGTE = !options.hasOwnProperty('gte') || str >= options.gte;
	let	checkLT = !options.hasOwnProperty('lt') || str < options.lt;
	let	checkGT = !options.hasOwnProperty('gt') || str > options.gt;

	return (regex.test(str) && checkLTE && checkGTE && checkLT && checkGT);
}

module.exports = isInt;
