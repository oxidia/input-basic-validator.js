const stringTypeError = require('./tools/stringTypeError');

function matches(str, pattern, modifiers) {
    stringTypeError(str);

    if (!(pattern instanceof RegExp)) {
        pattern = new RegExp(pattern, modifiers);
    }
    return (pattern.test(str));
}

module.exports = matches;
