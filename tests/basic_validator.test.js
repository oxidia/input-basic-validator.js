const bv = require('../index');
const stringTypeError = require('../lib/tools/stringTypeError');

describe('stringTypeError', () => {
	it('Valid string.', () => {
		stringTypeError('hello world');
	});
	it('Invalid string.', () => {
		try {
			stringTypeError(null);
		}
		catch (e) {
			expect(e.message).toContain('Expected');
		}
	});
});

describe('isEmpty', () => {
	it('Empty string.', () => {
		expect(bv.isEmpty('')).toBe(true);
	});
	it('Not empty string.', () => {
		expect(bv.isEmpty('str')).toBe(false);
	});
	it('string contains only spaces with ignore_space flag.', () => {
		expect(bv.isEmpty('   ', { ignore_space: true })).toBe(true);
	});
	it('string contains only spaces without ignore_space flag.', () => {
		expect(bv.isEmpty('   ', )).toBe(false);
	});
	it('Not empty string with ignore_space flag.', () => {
		expect(bv.isEmpty('  str ', { ignore_space: true })).toBe(false);
	});
});

describe('isNumeric', () => {
	it('Valid string number.', () => {
		expect(bv.isNumeric('-123.456')).toBe(true);
	});
	it('Invalid string number.', () => {
		expect(bv.isNumeric('++123.456')).toBe(false);
	});
	it('Valid string number with no_symbols flag.', () => {
		expect(bv.isNumeric('123', { no_symbols: true })).toBe(true);
	});
	it('Invalid string number with no_symbols flag.', () => {
		expect(bv.isNumeric('-123', { no_symbols: true })).toBe(false);
	});
});

describe('contains', () => {
	it('string starts with.', () => {
		expect(bv.contains('hello world', 'hello')).toBe(true);
	});
	it('string ends with.', () => {
		expect(bv.contains('hello world', 'world')).toBe(true);
	});
	it('string contains.', () => {
		expect(bv.contains('hello world', 'lo wo')).toBe(true);
	});
	it('string contains with case_sensitive flag set to true.', () => {
		expect(bv.contains('hello world', 'Lo Wo', { case_sensitive: true })).toBe(false);
	});
	it('string contains with case_sensitive flag set to false.', () => {
		expect(bv.contains('hello world', 'Lo Wo', { case_sensitive: false })).toBe(true);
	});
});

describe('equals', () => {
	it('Equal strings.', () => {
		expect(bv.equals('hello world', 'hello world')).toBe(true);
	});
	it('Not equal string.', () => {
		expect(bv.equals('hello world', 'world')).toBe(false);
	});
	it('Case insensitive comparaison', () => {
		expect(bv.equals('abc', 'ABC', { case_sensitive: false })).toBe(true);
	});
});

describe('isInt', () => {
	it('Valid int', () => {
		expect(bv.isInt('-123')).toBe(true);
	});
	it('Invalid int', () => {
		expect(bv.isInt('123xx')).toBe(false);
	});
	it('Valid With lt flag', () => {
		expect(bv.isInt('9', { lt: 10 })).toBe(true);
	});
	it('Invalid With lt flag', () => {
		expect(bv.isInt('10', { lt: 10 })).toBe(false);
	});
	it('Valid With gt flag', () => {
		expect(bv.isInt('11', { gt: 10 })).toBe(true);
	});
	it('Invalid With gt flag', () => {
		expect(bv.isInt('10', { gt: 10 })).toBe(false);
	});
	it('Valid With lte flag', () => {
		expect(bv.isInt('10', { lte: 10 })).toBe(true);
	});
	it('Invalid With lte flag', () => {
		expect(bv.isInt('11', { lte: 10 })).toBe(false);
	});
	it('Valid With gte flag', () => {
		expect(bv.isInt('10', { gte: 10 })).toBe(true);
	});
	it('Invalid With gte flag', () => {
		expect(bv.isInt('-1', { gte: 0 })).toBe(false);
	});
	it('Valid big number with flag lt', () => {
		expect(bv.isInt('2193819283298390832921289998', { lt: 1000 })).toBe(false);
	});
	it('Valid with gt and lt', () => {
		expect(bv.isInt('9', { lt: 10, gt: 0 })).toBe(true);
	});
	it('Invalid with gt and lt', () => {
		expect(bv.isInt('-1', { lt: 10, gt: 0 })).toBe(false);
	});
	it('Valid with gte and lte', () => {
		expect(bv.isInt('10', { lte: 10, gte: 10 })).toBe(true);
	});
	it('Invalid with gte and lte', () => {
		expect(bv.isInt('-1', { lte: 10, gte: 0 })).toBe(false);
	});
});

describe('isEmail', () => {
	// /^[a-z0-9.%_+-]+@[a-z0-9-]+\.[a-z0-9]{2,}$/i
	it('Valid simple email.', () => {
		expect(bv.isEmail('example@email.com')).toBe(true);
	});
	it('Valid email with special characters.', () => {
		expect(bv.isEmail('exa.%_+-@email.com')).toBe(true);
	});
	it('Valid email with ignore_max_length flag set to true.', () => {
		expect(bv.isEmail('Lorem_Lorem_Ipsum_is_simply_dummy_text_of_the_printing_and_typesetting_industry__Lorem_Ipsum_has_been_the_industry_s_standard_dummy_text_ever_since_the_1500s__when_an_unknown_printer_took_a_galley_of_type_and_scrambled_it_to_make_a_type_specimen_book@email.com', { ignore_max_length: true })).toBe(true);
	});
	it('Valid email with ignore_max_length flag set to false.', () => {
		expect(bv.isEmail('Lorem_Lorem_Ipsum_is_simply_dummy_text_of_the_printing_and_typesetting_industry__Lorem_Ipsum_has_been_the_industry_s_standard_dummy_text_ever_since_the_1500s__when_an_unknown_printer_took_a_galley_of_type_and_scrambled_it_to_make_a_type_specimen_book@email.com', { ignore_max_length: false })).toBe(false);
	});
	it('Valid email with more than one period(.) after "@"', () => {
		expect(bv.isEmail('jdoe@test.42.com')).toBe(true);
	});
	it('Invalid email missing character "@".', () => {
		expect(bv.isEmail('exampleemail.com')).toBe(false);
	});
	it('Invalid email missing character ".".', () => {
		expect(bv.isEmail('example@emailcom')).toBe(false);
	});
	it('Invalid email extention has less than two characters.', () => {
		expect(bv.isEmail('example@email.c')).toBe(false);
	});
	it('Invalid email zero character before "@".', () => {
		expect(bv.isEmail('@email.c')).toBe(false);
	});
	it('Invalid email zero character between "@" and ".".', () => {
		expect(bv.isEmail('example@.c')).toBe(false);
	});
});

describe('matches', () => {
	it('Basic valid test.', () => {
		expect(bv.matches('123', /[0-9]+/)).toBe(true);
	});
	it('Basic invalid test.', () => {
		expect(bv.matches('123a', /^[0-9]+$/)).toBe(false);
	});
	it('Test modifiers.', () => {
		expect(bv.matches('ABC', '[a-z]+', 'i')).toBe(true);
	});
});
